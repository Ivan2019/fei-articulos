package ec.com.fei.facturacionelectronica.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.com.fei.facturacionelectronica.entity.OrdenCompraEntity;

@Repository
public interface IOrdenCompraRepository extends JpaRepository<OrdenCompraEntity, String> {
	
	//OrdenCompraEntity obtenerOrdenCompra(Integer codigoOrdenCompra);

}
