package ec.com.fei.facturacionelectronica.model;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class OrdenCompraResponse {
	
	private Integer codigo;
	private Timestamp fecha;

}
