/**
 * 
 */
package ec.com.fei.facturacionelectronica.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

/**
 * @author Ivan
 *
 */
@Data
@Entity(name="ordencompra")
public class OrdenCompraEntity {
	@Id
	@Column(name="codigo")
	private Integer codigo;
	@Column(name="fecha")
	private Timestamp fecha;
	@Column(name = "total")
	private BigDecimal total;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "ordenCompra")
	private List<DetalleOrdenEntity> detalles;

}
