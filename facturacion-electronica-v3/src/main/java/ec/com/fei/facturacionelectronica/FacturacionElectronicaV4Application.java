package ec.com.fei.facturacionelectronica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FacturacionElectronicaV4Application {

	public static void main(String[] args) {
		SpringApplication.run(FacturacionElectronicaV4Application.class, args);
	}

}
