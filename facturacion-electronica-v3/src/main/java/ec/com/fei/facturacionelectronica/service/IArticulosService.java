package ec.com.fei.facturacionelectronica.service;

import java.util.Collection;

import ec.com.fei.facturacionelectronica.entity.ArticulosEntity;

public interface IArticulosService {
	Collection<ArticulosEntity> obtenerArticulos();
	void registrarArticulo(ArticulosEntity articulo);
	ArticulosEntity obternerArticulosId(Integer codigo);
	Collection<ArticulosEntity> obtenerArticulosPorEstado(String estado);
}
