package ec.com.fei.facturacionelectronica.model;

import lombok.Data;

@Data
public class ArticulosResponse {
	
	private Integer codigo;
	private String nombre;

}
