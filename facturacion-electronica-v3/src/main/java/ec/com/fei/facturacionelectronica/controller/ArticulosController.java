/**
 * 
 */
package ec.com.fei.facturacionelectronica.controller;

import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.com.fei.facturacionelectronica.entity.ArticulosEntity;
import ec.com.fei.facturacionelectronica.service.ArticulosService;

/**
 * @author Ivan
 *
 */
@RestController
@RequestMapping("/articulos")

public class ArticulosController {
	
	private static final Log LOOGGER = LogFactory.getLog(ArticulosController.class);
	@Autowired
	
	private ArticulosService articulosService; 
	
	@GetMapping("/obtenerArticulos")
	public Collection<ArticulosEntity> obtenerArticulos()
	{
		Collection<ArticulosEntity> articulos = articulosService.obtenerArticulos();
		LOOGGER.info(articulos.iterator().next().getNombre());
		return articulos;
	}
	@PostMapping("/registrarArticulos")
	public void registrarArticulos(@RequestBody ArticulosEntity articulos) {
			articulosService.registrarArticulo(articulos);
		
	}
	
	public ArticulosEntity obtenerArticulosId(Integer codigo)
	{
		return articulosService.obternerArticulosId(codigo);
	
	}
	@GetMapping("/obtenerarticulosporestado")
	public Collection<ArticulosEntity> obtenerArticulosPorEstado(String estado){
		return articulosService.obtenerArticulosPorEstado(estado);
	}
		
	
}
