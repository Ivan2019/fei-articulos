package ec.com.fei.facturacionelectronica.service;

import java.util.Collection;

import ec.com.fei.facturacionelectronica.entity.OrdenCompraEntity;

public interface IOrdenCompraService {
	Collection<OrdenCompraEntity> obtenerOrdenCompra();
}
