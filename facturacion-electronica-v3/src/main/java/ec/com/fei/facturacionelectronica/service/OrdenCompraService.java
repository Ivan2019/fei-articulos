package ec.com.fei.facturacionelectronica.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.com.fei.facturacionelectronica.entity.OrdenCompraEntity;
import ec.com.fei.facturacionelectronica.repository.IOrdenCompraRepository;

@Service
public class OrdenCompraService implements IOrdenCompraService {

	@Autowired
	private IOrdenCompraRepository ordencompra;

	@Override
	public Collection<OrdenCompraEntity> obtenerOrdenCompra() {
		// TODO Auto-generated method stub
		return ordencompra.findAll();
	}

}
