package ec.com.fei.facturacionelectronica.model;

import org.springframework.stereotype.Component;

import ec.com.fei.facturacionelectronica.entity.OrdenCompraEntity;

@Component
public class OrdenCompraConverter {
	
	public OrdenCompraResponse convertOrdenCompraResponse(OrdenCompraEntity ordenCompra){
		
		OrdenCompraResponse ordenCompraResponse=new OrdenCompraResponse();
		ordenCompraResponse.setCodigo(ordenCompra.getCodigo());
		ordenCompraResponse.setFecha(ordenCompra.getFecha());
		return ordenCompraResponse;
	}
}
