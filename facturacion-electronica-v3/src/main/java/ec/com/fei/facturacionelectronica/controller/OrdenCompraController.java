package ec.com.fei.facturacionelectronica.controller;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.com.fei.facturacionelectronica.entity.OrdenCompraEntity;
import ec.com.fei.facturacionelectronica.model.OrdenCompraConverter;
import ec.com.fei.facturacionelectronica.model.OrdenCompraResponse;
import ec.com.fei.facturacionelectronica.service.OrdenCompraService;

@RestController
@RequestMapping("/ordencompra")
public class OrdenCompraController {
	
	@Autowired
	private OrdenCompraService ordenCompraService;
	
	@Autowired
	private OrdenCompraConverter ordenCompraConverter;
	
	@GetMapping("/obtenerorden")
	public Collection<OrdenCompraResponse> obtenerOrdenCompra(){
		Collection<OrdenCompraResponse> ordenCompraRes=new ArrayList<>();
		ordenCompraService.obtenerOrdenCompra().forEach(orden->{
			ordenCompraRes.add(ordenCompraConverter.convertOrdenCompraResponse(orden));
		});;
		return ordenCompraRes;
	}
	
	


}
