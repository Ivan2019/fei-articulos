package ec.com.fei.facturacionelectronica.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ec.com.fei.facturacionelectronica.entity.ArticulosEntity;

@Repository("articulosRepository")
public interface IArticulosRepository extends JpaRepository<ArticulosEntity, Serializable> {

}
