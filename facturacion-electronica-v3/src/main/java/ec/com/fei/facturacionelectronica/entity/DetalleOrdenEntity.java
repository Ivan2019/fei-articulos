/**
 * 
 */
package ec.com.fei.facturacionelectronica.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

/**
 * @author Ivan
 *
 */
@Data
@Entity(name="detalleordencompra")
public class DetalleOrdenEntity {
	
	@Id
	@Column(name="codigo")
	private Integer codigo;
	
	@Column(name="cantidad")
	private BigDecimal cantidad;
	
//	@Column(name="codigo_articulo")
//	private Integer codigo_articulo;
	
//	@Column(name="codigo_ordencompra")
//	private Integer codigo_ordencompra;
	
	@ManyToOne
	@JoinColumn(name="codigo_articulo",nullable = false)
	private ArticulosEntity articulo;
	
	@ManyToOne
	@JoinColumn(name="codigo_ordencompra", nullable = false)
	private OrdenCompraEntity ordenCompra;
	

}
