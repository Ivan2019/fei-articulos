package ec.com.fei.facturacionelectronica.repository;

import java.util.Collection;

import ec.com.fei.facturacionelectronica.entity.ArticulosEntity;

public interface IArticulosRepositoryQuerySdl {
	ArticulosEntity obtenerArticulo(Integer codigo);
	
	Collection<ArticulosEntity> obtenerArticulosPorEstado(String estado);
	
}
