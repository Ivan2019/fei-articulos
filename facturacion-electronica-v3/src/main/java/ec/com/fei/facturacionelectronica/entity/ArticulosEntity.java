/**
 * 
 */
package ec.com.fei.facturacionelectronica.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

/**
 * @author Ivan
 *
 */
@Data
@Entity(name="articulos")
public class ArticulosEntity {
	
	@Id
	@Column(name="codigo")
	private Integer codigo;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="estado")
	private String estado;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "articulo")
	private List<DetalleOrdenEntity> detalles;

}
