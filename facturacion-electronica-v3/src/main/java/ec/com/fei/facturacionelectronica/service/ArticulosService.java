/**
 * 
 */
package ec.com.fei.facturacionelectronica.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.com.fei.facturacionelectronica.entity.ArticulosEntity;
import ec.com.fei.facturacionelectronica.repository.IArticulosRepository;
import ec.com.fei.facturacionelectronica.repository.IArticulosRepositoryQuerySdl;

/**
 * @author Ivan
 *
 */
@Service("articulosService")
public class ArticulosService implements IArticulosService{

	@Autowired
	private IArticulosRepository articulosRepository;

	@Autowired
	private IArticulosRepositoryQuerySdl articulosRepositoryQuerySdl;
	public Collection<ArticulosEntity> obtenerArticulos() {
		// TODO Auto-generated method stub
		return articulosRepository.findAll();
	}

	@Override
	public void registrarArticulo(ArticulosEntity articulo) {
		// TODO Auto-generated method stub
		articulosRepository.save(articulo);
	}

	@Override
	public ArticulosEntity obternerArticulosId(Integer codigo) {
		// TODO Auto-generated method stub
		return articulosRepositoryQuerySdl.obtenerArticulo(codigo);
	}

	@Override
	public Collection<ArticulosEntity> obtenerArticulosPorEstado(String estado) {
		// TODO Auto-generated method stub
		return articulosRepositoryQuerySdl.obtenerArticulosPorEstado(estado);
	}

}
