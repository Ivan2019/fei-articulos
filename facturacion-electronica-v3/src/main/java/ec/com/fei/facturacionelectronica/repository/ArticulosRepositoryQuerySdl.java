package ec.com.fei.facturacionelectronica.repository;

import static com.querydsl.core.types.Projections.bean;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.querydsl.jpa.impl.JPAQuery;

import ec.com.fei.facturacionelectronica.entity.ArticulosEntity;
import ec.com.fei.facturacionelectronica.entity.QArticulosEntity;

@Repository
public class ArticulosRepositoryQuerySdl implements IArticulosRepositoryQuerySdl {

	private QArticulosEntity qArticulosEntity = QArticulosEntity.articulosEntity;

	@PersistenceContext
	private EntityManager em;

	public ArticulosEntity obtenerArticulo(Integer codigo) {
		// TODO Auto-generated method stub
		JPAQuery<ArticulosEntity> query = new JPAQuery<ArticulosEntity>(em);

		// ArticulosEntity articulosEntity =
		// query.select(qArticulosEntity).from(qArticulosEntity)
		// .where(qArticulosEntity.codigo.eq(codigo)).fetchOne();
		// return articulosEntity;
		return query.from(qArticulosEntity).select(bean(ArticulosEntity.class, qArticulosEntity.nombre))
				.where(qArticulosEntity.codigo.eq(codigo)).fetchOne();
	}

	public Collection<ArticulosEntity> obtenerArticulosPorEstado(String estado) {
		JPAQuery<ArticulosEntity> queryDsl=new JPAQuery<ArticulosEntity>(em);
		
		return queryDsl.from(qArticulosEntity).select(bean(ArticulosEntity.class,qArticulosEntity.codigo)).where(qArticulosEntity.estado.eq(estado)).fetch();
	}

}
